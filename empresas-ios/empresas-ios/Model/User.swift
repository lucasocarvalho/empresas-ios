//
//  User.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

public struct User {
    var accessToken: String
    var client: String
    var uid: String
}
