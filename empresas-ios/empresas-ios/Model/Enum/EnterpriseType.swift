//
//  EnterpriseType.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

public struct EnterpriseType: Codable {
    public var id: Int
    public var enterprise_type_name: String
}
