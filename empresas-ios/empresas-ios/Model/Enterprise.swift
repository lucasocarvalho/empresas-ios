//
//  Enterprise.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

public struct Enterprises: Codable {
    var enterprises: [Enterprise]
}

public struct Enterprise: Codable {
    var id: Int
    var email_enterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool
    var enterprise_name: String
    var photo: String
    var description: String
    var city: String
    var country: String
    var value: Int
    var share_price: Double
    var enterprise_type: EnterpriseType
}
