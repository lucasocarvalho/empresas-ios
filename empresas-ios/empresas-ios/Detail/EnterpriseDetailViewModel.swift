//
//  EnterpriseDetailViewModel.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class EnterpriseDetailViewModel {
    //*************************************************
    // MARK: - Private Properties
    //*************************************************
    
    private var enterprise: Enterprise
    
    //*************************************************
    // MARK: - Public Properties
    //*************************************************

    var enterpriseName: String {
        return enterprise.enterprise_name
    }
    
    var enterpriseDescription: String {
        return enterprise.description
    }
    
    //*************************************************
    // MARK: - Inits
    //*************************************************

    init(enterprise: Enterprise) {
        self.enterprise = enterprise
    }
}
