//
//  EnterpriseDetailViewController.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class EnterpriseDetailViewController: UIViewController {
    //*************************************************
    // MARK: - Outlets
    //*************************************************
    
    @IBOutlet private weak var brandView: UIView!
    @IBOutlet private weak var enterpriseNameLabel: UILabel!
    @IBOutlet private weak var enterpriseDescriptionLabel: UILabel!
    
    //*************************************************
    // MARK: - Private Properties
    //*************************************************

    private var viewModel: EnterpriseDetailViewModel?
    
    //*************************************************
    // MARK: - Life cycle
    //*************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIData()
    }
}

//*************************************************
// MARK: - Public Methods
//*************************************************

extension EnterpriseDetailViewController {
    func setup(with viewModel: EnterpriseDetailViewModel) {
        self.viewModel = viewModel
    }
}

//*************************************************
// MARK: - Private Methods
//*************************************************

extension EnterpriseDetailViewController {
    private func setupUIData() {
        guard let viewModel: EnterpriseDetailViewModel = viewModel else { return }

        enterpriseNameLabel.text = viewModel.enterpriseName
        enterpriseDescriptionLabel.text = viewModel.enterpriseDescription
        
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        guard let viewModel: EnterpriseDetailViewModel = viewModel else { return }

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationItem.hidesBackButton = false
        self.navigationItem.title = viewModel.enterpriseName.uppercased()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
    }
}
