//
//  LoginViewModel.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class LoginViewModel {
    //*************************************************
    // MARK: - Private Properties
    //*************************************************

    private let userService: UserServiceProtocol
    
    //*************************************************
    // MARK: - Inits
    //*************************************************

    init(userService: UserServiceProtocol) {
        self.userService = userService
    }
}

//*************************************************
// MARK: - Public Methods
//*************************************************

extension LoginViewModel {
    func signIn(with email: String, with password: String, completion: @escaping ((User?, Error?) ->Void)) {
        userService.signIn(email: email, password: password) { (user, error) in
            if let user: User = user {
                completion(user, nil)
            } else if let error: Error = error {
                completion (nil, error)
            } else {
                completion(nil, nil)
            }
        }
    }
}
