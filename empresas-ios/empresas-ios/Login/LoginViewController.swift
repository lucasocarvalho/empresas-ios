//
//  ViewController.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class LoginViewController: UIViewController {
    
    //*************************************************
    // MARK: - Outlets
    //*************************************************
    
    @IBOutlet weak var ioasysImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var warningMessageLabel: UILabel!
    
    //*************************************************
    // MARK: - Private Properties
    //*************************************************

    private let viewModel: LoginViewModel = LoginViewModel(userService: UserService())

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
}

//*************************************************
// MARK: - Actions
//*************************************************

extension LoginViewController {
    @IBAction func login(_ sender: UIButton) {
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            warningMessageLabel.isHidden = false
            return
        }
        
        viewModel.signIn(with: email, with: password) { (user, error) in
            if let user: User = user {
                let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                if let homeViewController: HomeViewController = storyboard.instantiateViewController(identifier: "HomeViewController") as? HomeViewController {
                    UserDefaults.standard.setValue(user.accessToken, forKey: "accessToken")
                    UserDefaults.standard.setValue(user.client, forKey: "client")
                    UserDefaults.standard.setValue(user.uid, forKey: "uid")
                    self.navigationController?.pushViewController(homeViewController, animated: true)
                }
            } else if let error: Error = error {
                let alert: UIAlertController = UIAlertController(title: "Erro de login", message: error.localizedDescription, preferredStyle: .alert)
                let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

//*************************************************
// MARK: - Private Methods
//*************************************************

extension LoginViewController {
    private func setupUI() {
        warningMessageLabel.isHidden = true
        loginButton.layer.cornerRadius = 8.0
        ioasysImageView.clipsToBounds = true
        ioasysImageView.layer.cornerRadius = 40
        ioasysImageView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
}

//*************************************************
// MARK: - TextFie
//*************************************************

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        warningMessageLabel.isHidden = true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        warningMessageLabel.isHidden = true
    }
}
