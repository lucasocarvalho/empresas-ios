//
//  User+Endpoint.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 05/07/21.
//

import Foundation

public enum UserEndpoint: RequestableProtocol {
    case signIn(email: String, password: String)
    
    public var host: String {
        return "https://empresas.ioasys.com.br/api"
    }
    
    public var path: String {
        switch self {
        case .signIn:
            return "/v1/users/auth/sign_in"
        }
    }
    
    public var body: [String : String] {
        switch self {
        case .signIn(let email, let password):
            return [
                "email": email,
                "password": password
            ]
        }
    }
    
    public var titleError: String {
        switch self {
        case .signIn:
            return "Sign In Error"
        }
    }
}
