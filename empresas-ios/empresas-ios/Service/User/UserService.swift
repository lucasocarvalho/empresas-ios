//
//  UserService.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 05/07/21.
//

import UIKit
import Alamofire

/// Protocol to control user service
public protocol UserServiceProtocol {
    /// Sign in a user
    ///
    /// - parameters:
    /// - email: user's email
    /// - password: user's password
    /// - completionHandler: user logged or error
    func signIn(email: String, password: String, completionHandler: @escaping (User?, Error?) -> ())
}

public class UserService: UserServiceProtocol {
    public func signIn(email: String, password: String, completionHandler: @escaping (User?, Error?) -> ()) {
        let userEndpoint: UserEndpoint = UserEndpoint.signIn(email: email, password: password)
        let url: String = userEndpoint.host + userEndpoint.path
        let body: [String : String] = userEndpoint.body
        
        AF.request(url, method: .post, parameters: body)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response) in
                switch response.result {
                    case .success:
                        if let response: HTTPURLResponse = response.response, let accessToken: String = response.allHeaderFields["access-token"] as? String, let client: String = response.allHeaderFields["client"] as? String, let uid = response.allHeaderFields["uid"] as? String {
                            let user: User = User(accessToken: accessToken, client: client, uid: uid)
                            completionHandler(user, nil)
                        }
                    case .failure(let error):
                        completionHandler(nil, error)
                }
            }
    }
}
