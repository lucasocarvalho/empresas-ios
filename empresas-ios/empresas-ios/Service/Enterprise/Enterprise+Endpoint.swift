//
//  Enterprise+Endpoint.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 05/07/21.
//

import Foundation
import Alamofire

public enum EnterpriseEndpoint: RequestableProtocol {
    case searchEnterprise(name: String, user: User)
    
    public var host: String {
        return "https://empresas.ioasys.com.br/api"
    }
    
    public var path: String {
        switch self {
        case .searchEnterprise(let name, _):
            return "/v1/enterprises?name=\(name)"
        }
    }
    
    public var body: [String : String] {
        return [:]
    }
    
    public var headers: HTTPHeaders {
        switch self {
        case .searchEnterprise(_, let user):
            return [
                .init(name: "access-token", value: user.accessToken),
                .init(name: "client", value: user.client),
                .init(name: "uid", value: user.uid)
            ]
        }
    }
    
    public var titleError: String {
        return "Search Enterprise"
    }
    
}
