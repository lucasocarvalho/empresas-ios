//
//  EnterpriseService.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 05/07/21.
//

import UIKit
import Alamofire

/// Protocol to control enterprise service
public protocol EnterpriseServiceProtocol {
    /// Searches for enterprise in API
    ///
    /// - parameters:
    /// - email: enterprise name
    /// - user: user logged
    /// - completionHandler: list of enterprises returned
    func searchEnterprise(name: String, user: User, completionHandler: @escaping ([Enterprise]) -> ())
}

public class EnterpriseService: EnterpriseServiceProtocol {
    public func searchEnterprise(name: String, user: User, completionHandler: @escaping ([Enterprise]) -> ()) {
        let enterpriseEndpoint: EnterpriseEndpoint = EnterpriseEndpoint.searchEnterprise(name: name, user: user)
        let url: String = enterpriseEndpoint.host + enterpriseEndpoint.path
        let headers: HTTPHeaders = enterpriseEndpoint.headers
        
        AF.request(url, method: .get, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    do {
                        let enterprises: Enterprises = try JSONDecoder().decode(Enterprises.self, from: data)
                        completionHandler(enterprises.enterprises)
                    } catch {
                        completionHandler([])
                    }
                case .failure:
                    completionHandler([])
                }
            }
    }
}
