//
//  RequestableProtocol.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 05/07/21.
//

import Foundation

/// Protocol to handle API requests
public protocol RequestableProtocol {
    var host: String { get }
    var path: String { get }
    var body: [String : String] { get }
    var titleError: String { get }
}
