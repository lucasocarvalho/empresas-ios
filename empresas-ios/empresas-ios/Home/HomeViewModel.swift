//
//  HomeViewModel.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class HomeViewModel {
    //*************************************************
    // MARK: - Private Properties
    //*************************************************
    
    private let enterpriseService: EnterpriseServiceProtocol
    private var enterprises: [Enterprise] = []
    
    //*************************************************
    // MARK: - Public Properties
    //*************************************************

    public var numberOfEnterprises: Int = 0

    //*************************************************
    // MARK: - Inits
    //*************************************************

    init(enterpriseService: EnterpriseServiceProtocol) {
        self.enterpriseService = enterpriseService
    }
}

//*************************************************
// MARK: - Public Methods
//*************************************************

extension HomeViewModel {
    func searchEnterprise(for name: String, user: User) {
        enterpriseService.searchEnterprise(name: name, user: user) { (enterprises) in
            if enterprises.count > 0 {
                self.enterprises = enterprises
                self.numberOfEnterprises = enterprises.count
            } else {
                self.enterprises = []
            }
        }
    }
    
    func enterpriseForIndex(index: Int) -> Enterprise? {
        if index < enterprises.count {
            return enterprises[index]
        }
        return nil
    }
}
