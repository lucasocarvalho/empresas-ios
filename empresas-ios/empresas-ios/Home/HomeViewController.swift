//
//  HomeViewController.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class HomeViewController: UIViewController {
    
    //*************************************************
    // MARK: - Outlets
    //*************************************************
    
    @IBOutlet private weak var numberOfResultsLabel: UILabel!
    @IBOutlet private weak var resultsTableView: UITableView!
    
    //*************************************************
    // MARK: - Private Properties
    //*************************************************
    
    private var viewModel: HomeViewModel?
    private let searchController: UISearchController = UISearchController(searchResultsController: nil)
    
    //*************************************************
    // MARK: - Public Properties
    //*************************************************
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isSearchBarFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
        
    //*************************************************
    // MARK: - Life cycle
    //*************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = HomeViewModel(enterpriseService: EnterpriseService())
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
    }
}

//*************************************************
// MARK: - Private Methods
//*************************************************

extension HomeViewController {
    private func setupUI() {
        self.numberOfResultsLabel.text = "\(viewModel?.numberOfEnterprises ?? 0) resultados encontrados"
        setupTableView()
        setupNavigationBar()
        setupSearchController()
    }
    
    private func setupNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        let navigationBackgroundImage: UIImage = UIImage(named: "home-bg")!
        self.navigationController?.navigationBar.setBackgroundImage(navigationBackgroundImage, for: .defaultPrompt)
    }
    
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Pesquise por empresa"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    private func setupTableView() {
        resultsTableView.dataSource = self
        resultsTableView.delegate = self
        resultsTableView.register(UINib(nibName: "EnterpriseTableViewCell", bundle: .main), forCellReuseIdentifier: "EnterpriseTableViewCell")
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        if let viewModel: HomeViewModel = viewModel, let accessToken: String = UserDefaults.standard.value(forKey: "accessToken") as? String, let client: String = UserDefaults.standard.value(forKey: "client") as? String, let uid: String = UserDefaults.standard.value(forKey: "uid") as? String {
            
            let user: User = User(accessToken: accessToken, client: client, uid: uid)
            viewModel.searchEnterprise(for: searchText, user: user)
            
            self.numberOfResultsLabel.text = "\(viewModel.numberOfEnterprises) resultados encontrados"
            
            DispatchQueue.main.async {
                self.resultsTableView.reloadData()
            }
        }
    }
}

//*************************************************
// MARK: - UITableViewDataSource, UITableViewDelegate
//*************************************************

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel: HomeViewModel = viewModel else { return 0 }
        return viewModel.numberOfEnterprises
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let viewModel: HomeViewModel = viewModel, let cell: EnterpriseTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EnterpriseTableViewCell", for: indexPath) as? EnterpriseTableViewCell, let enterprise: Enterprise = viewModel.enterpriseForIndex(index: indexPath.row) {
            let cellViewModel: EnterpriseTableViewCellViewModel = EnterpriseTableViewCellViewModel(enterprise: enterprise)
            cell.setup(with: cellViewModel)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "EnterpriseDetail", bundle: nil)
        guard let enterpriseDetailViewController: EnterpriseDetailViewController = storyboard.instantiateViewController(identifier: "EnterpriseDetailVC") as? EnterpriseDetailViewController, let viewModel: HomeViewModel = viewModel else { return }
        
        if let enterprise: Enterprise = viewModel.enterpriseForIndex(index: indexPath.row) {
            let enterpriseDetailViewModel: EnterpriseDetailViewModel = EnterpriseDetailViewModel(enterprise: enterprise)
            enterpriseDetailViewController.setup(with: enterpriseDetailViewModel)
            self.navigationController?.pushViewController(enterpriseDetailViewController, animated: true)
        }
    }
}

//*************************************************
// MARK: - UISearchResultsUpdating
//*************************************************

extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        if let searchBarText: String = searchBar.text {
            filterContentForSearchText(searchBarText)
        }
    }
}
