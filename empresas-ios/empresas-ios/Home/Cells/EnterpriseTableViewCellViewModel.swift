//
//  EnterpriseTableViewCellViewModel.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class EnterpriseTableViewCellViewModel {
    //*************************************************
    // MARK: - Private Properties
    //*************************************************

    private var enterprise: Enterprise
    
    //*************************************************
    // MARK: - Public Properties
    //*************************************************
    
    var enterpriseName: String {
        return enterprise.enterprise_name
    }
    
    //*************************************************
    // MARK: - Inits
    //*************************************************
    
    init(enterprise: Enterprise) {
        self.enterprise = enterprise
    }
}
