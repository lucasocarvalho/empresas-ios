//
//  EnterpriseTableViewCell.swift
//  empresas-ios
//
//  Created by Lucas Carvalho on 27/06/21.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    //*************************************************
    // MARK: - Outlets
    //*************************************************
    
    @IBOutlet private weak var enterpriseBrandView: UIView!
    @IBOutlet private weak var enterpriseNameLabel: UILabel!
    
    //*************************************************
    // MARK: - Private Properties
    //*************************************************
    
    private var viewModel: EnterpriseTableViewCellViewModel?

    //*************************************************
    // MARK: - Life cycle
    //*************************************************

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
}

//*************************************************
// MARK: - Public Methods
//*************************************************

extension EnterpriseTableViewCell {
    func setup(with viewModel: EnterpriseTableViewCellViewModel) {
        self.viewModel = viewModel
        setupUIData()
    }

}

//*************************************************
// MARK: - Private Methods
//*************************************************

extension EnterpriseTableViewCell {
    func setupUI() {
        enterpriseBrandView.layer.cornerRadius = 4.0
    }
    
    func setupUIData() {
        guard let viewModel: EnterpriseTableViewCellViewModel = viewModel else { return }
        enterpriseNameLabel.text = viewModel.enterpriseName
    }
}
